﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakeNews.Models
{
    public class NewsItem
    {
        public int ID { get; set; }
        public string Category { get; set; }
        public string Headline { get; set; }
        public string Subhead { get; set; }
        public string Dateline { get; set; }
        public string Image { get; set; }

    }

    public class NewsManager
    {
        public static void GetNews(string category, ObservableCollection<NewsItem> NewsItems)
        {
            var allItems = getNewsItems();

            var filteredNewsItems = allItems.Where(p => p.Category == category).ToList();

            NewsItems.Clear();

            filteredNewsItems.ForEach(p => NewsItems.Add(p));

        }

        public static List<NewsItem> getNewsItems()
        {
            var items = new List<NewsItem>();

            items.Add(new NewsItem() { ID = 1, Category = "Financial", Headline = "Test 1", Subhead = "Final 1", Image = "Assets/Financial.png" });

            items.Add(new NewsItem() { ID = 2, Category = "Financial", Headline = "Test 2", Subhead = "Final 2", Image = "Assets/Financial3.png" });

            items.Add(new NewsItem() { ID = 3, Category = "Financial", Headline = "Test 3", Subhead = "Final 3", Image = "Assets/Financial4.png" });

            items.Add(new NewsItem() { ID = 4, Category = "Financial", Headline = "Test 4", Subhead = "Final 4", Image = "Assets/Financial5.png" });
                
            items.Add(new NewsItem() { ID = 5, Category = "Financial", Headline = "Test 5", Subhead = "Final 5", Image = "Assets/Food.png" });

            items.Add(new NewsItem() { ID = 6, Category = "Food", Headline = "Test 6", Subhead = "Food 1", Image = "Assets/Food1.png" });

            items.Add(new NewsItem() { ID = 7, Category = "Food", Headline = "Test 7", Subhead = "Food 2", Image = "Assets/Food2.png" });

            items.Add(new NewsItem() { ID = 8, Category = "Food", Headline = "Test 8", Subhead = "Food 3", Image = "Assets/Food3.png" });

            items.Add(new NewsItem() { ID = 9, Category = "Food", Headline = "Test 9", Subhead = "Food 4", Image = "Assets/Food4.png" });

            items.Add(new NewsItem() { ID = 10, Category = "Food", Headline = "Test 10", Subhead = "Food 5", Image = "Assets/Food5.png" });

            return items;
        }
    }
}
